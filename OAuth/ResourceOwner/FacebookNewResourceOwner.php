<?php

namespace AT\UserBundle\OAuth\ResourceOwner;

use HWI\Bundle\OAuthBundle\OAuth\ResourceOwner\FacebookResourceOwner;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * FacebookResourceOwner
 *
 * @author Geoffrey Bachelet <geoffrey.bachelet@gmail.com>
 */
class FacebookNewResourceOwner extends FacebookResourceOwner
{

    /**
     * Update of FB API version to 2.5
     * {@inheritDoc}
     */
    protected function configureOptions(OptionsResolverInterface $resolver)
    {
        parent::configureOptions($resolver);

        $resolver->setDefaults(array(
            'authorization_url'   => 'https://www.facebook.com/v2.5/dialog/oauth',
            'access_token_url'    => 'https://graph.facebook.com/v2.5/oauth/access_token',
            'revoke_token_url'    => 'https://graph.facebook.com/v2.5/me/permissions',
            'infos_url'           => 'https://graph.facebook.com/v2.5/me?fields=id,first_name,last_name,email',

            'use_commas_in_scope' => true,

            'display'             => null,
            'auth_type'           => null,
            'appsecret_proof'     => false,
        ));

        // Symfony <2.6 BC
        if (method_exists($resolver, 'setDefined')) {
            $resolver
                ->setAllowedValues('display', array('page', 'popup', 'touch', null)) // @link https://developers.facebook.com/docs/reference/dialogs/#display
                ->setAllowedValues('auth_type', array('rerequest', null)) // @link https://developers.facebook.com/docs/reference/javascript/FB.login/
                ->setAllowedTypes('appsecret_proof', 'bool') // @link https://developers.facebook.com/docs/graph-api/securing-requests
            ;
        } else {
            $resolver->setAllowedValues(array(
                'display'   => array('page', 'popup', 'touch', null),
                'auth_type' => array('rerequest', null),
                'appsecret_proof' => array(true, false),
            ));
        }
    }
}
