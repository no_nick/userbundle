<?php

namespace AT\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ATUserBundle:Default:index.html.twig');
    }
}
